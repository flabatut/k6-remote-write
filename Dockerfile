FROM golang:alpine as builder

RUN apk update \
        && wget https://github.com/grafana/xk6/releases/download/v0.5.0/xk6_0.5.0_linux_amd64.tar.gz \
        && tar zxvf xk6_0.5.0_linux_amd64.tar.gz \
        && chmod +x xk6 \
        && mv xk6 /usr/local/bin/xk6 \
        && xk6 build --with github.com/grafana/xk6-output-prometheus-remote@latest

FROM alpine:latest
COPY --from=builder /go/k6 /usr/local/bin/k6
ENTRYPOINT ["/usr/local/bin/k6"]
